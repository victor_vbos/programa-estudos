#!/bin/sh
cd /var/www/html

cd storage/
mkdir framework/
cd framework
mkdir cache
mkdir sessions
mkdir views
cd ../../
chmod -R 777 storage/

echo "============ Iniciando o composer ============"
php composer.phar install
echo "============ Composer conclu�do ============"

#php artisan db:wipe
php artisan migrate:refresh
php artisan db:seed --class=DatabaseSeeder
#php artisan db:seed --class=QuestaoSeeder

exec $@

FROM php:7.4-fpm-alpine

WORKDIR /var/www/html

RUN docker-php-ext-install pdo pdo_mysql

COPY .docker/start.sh /usr/local/
RUN chmod +x /usr/local/start.sh

CMD ["/usr/local/start.sh", "php-fpm", "-F"]

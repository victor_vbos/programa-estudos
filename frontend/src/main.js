import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import { routes } from './router';
import Axios from 'axios';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueRouter);
Vue.prototype.$http=Axios;

import _ from 'lodash';
Object.defineProperty(Vue.prototype, '$_', { value: _ });

Vue.config.productionTip = false

const router = new VueRouter({
  mode: 'history',
  routes
});

export const eventBus = new Vue();

new Vue({
  el: '#app',
  router,
  render: h => h(App),
})

<?php

use App\Assunto;
use Illuminate\Database\Seeder;
use App\Database\DummyData;

class AssuntoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (DummyData::ASSUNTOS as $chave => $assunto) {
            Assunto::create([
                'nome' => $chave,
                'pai_id' => $assunto
            ]);
        }
    }
}

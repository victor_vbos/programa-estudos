<?php

use App\Assunto;
use App\Banca;
use App\Orgao;
use App\Questao;
use Illuminate\Database\Seeder;
use App\Database\DummyData;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(BancaSeeder::class);
         $this->call(OrgaoSeeder::class);
         $this->call(AssuntoSeeder::class);
         $this->call(QuestaoSeeder::class);
//         factory(Questao::class, 100)->create();
    }
}

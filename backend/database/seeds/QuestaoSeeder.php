<?php


use App\Questao;
use Illuminate\Database\Seeder;
use App\Database\DummyData;
use Illuminate\Support\Str;


class QuestaoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 4000; $i++) {
            DB::table('questao')->insert([
                'nome' => Str::random(10),
                'orgao_id' => rand(1, 10),
                'banca_id' => rand(1, 10),
                'assunto_id' => rand(3, 20),
            ]);
        }
    }
}

<?php

use App\Banca;
use Illuminate\Database\Seeder;
use App\Database\DummyData;

class BancaSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (DummyData::BANCAS as $banca) {
            Banca::create(['nome' => $banca]);
        }
    }
}

<?php

namespace App\Database;

class DummyData {
    const BANCAS = [
        'CESPE / CEBRASPE',
        'VUNESP',
        'IDIB',
        'Instituto Legatus',
        'AOCP',
        'FGV',
        'Quadrix',
        'SELECON',
        'IBGP',
        'IBAM',
    ];

    const ORGAOS = [
      'Marinha - Marinha do Brasil',
      'TRE-DF',
      'Defensoria Pública da União - DPU',
      'CREA',
      'Tribunal Regional Federal',
      'Caixa Econômica Federal',
      'PM SP',
      'TJ - DF',
      'SEFAZ',
      'SAAE',
    ];

    const ASSUNTOS = [
        'MATEMÁTICA' => null,
        'PORTUGUES' => null,
        'INFORMÁTICA' => null,
        'FRAÇÕES'     => 1,
        'RAZÃO E PROPORÇÃO'     => 1,
        'PORCENTAGEM'     => 1,
        'GÊNEROS TEXTUAIS' => 2,
        'TEXTOS NARRATIVOS' => 7,
        'TEXTOS DESCRITIVOS' => 7,
        'TEXTOS EXPOSITIVOS' => 7,
        'TEXTOS DISSERTATIVOS' => 8,
        'LEITURA E ARTES' => 2,
        'LÓGICA'  => null,
        'ATUALIDADES E CONHECIMENTOS GERAIS'  => null,
        'NAVEGADORES' => 3,
        'LINGUAGENS PROGRAMAÇÂO' => 3,
        'JAVA' => 16,
        'PHP' => 16,
        'PYTHON' => 16,
        'C' => 16,

    ];
}

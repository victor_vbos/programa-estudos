<?php


use App\Orgao;
use Illuminate\Database\Seeder;
use App\Database\DummyData;

class OrgaoSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (DummyData::ORGAOS as $orgao) {
            Orgao::create(['nome' => $orgao]);
        }
    }
}

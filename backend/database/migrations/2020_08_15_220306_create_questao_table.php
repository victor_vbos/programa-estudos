<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('assunto_id')->unsigned();
            $table->integer('orgao_id')->unsigned();
            $table->integer('banca_id')->unsigned();

            $table->softDeletes('data_exclusao');
            $table->timestamps();


        });

        Schema::table('questao', function($table) {
            $table->foreign('assunto_id')->references('id')->on('assunto');
            $table->foreign('orgao_id')->references('id')->on('orgao');
            $table->foreign('banca_id')->references('id')->on('banca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questao');
    }
}

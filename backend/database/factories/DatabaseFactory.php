<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Banca;
use App\Assunto;
use App\Orgao;
use App\Questao;
use Faker\Generator as Faker;

function getInstanceOf($class, $returnIdOnly = true) {
    if ($class == 'App\Assunto') {
        $instance = $class::inRandomOrder()->doesntHave('filhos')->first() ?? factory($class)->create();
    } else {
        $instance = $class::inRandomOrder()->first() ?? factory($class)->create();
//        dd('pq');
    }

    return $returnIdOnly ? $instance->id : $instance;
}

$factory->define(Questao::class, function (Faker $faker) {
    return [
        'nome' => $faker->text,
        'assunto_id' => getInstanceOf(Assunto::class),
        'orgao_id' => getInstanceOf(Orgao::class),
        'banca_id' => getInstanceOf(Banca::class),
    ];
});

<?php

namespace App\Http\Controllers;

use App\Banca;
use Illuminate\Http\Request;

class BancaController extends Controller
{
    public function list()
    {
        $bancas = Banca::all();

        return response()->json($bancas);
    }
}

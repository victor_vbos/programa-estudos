<?php

namespace App\Http\Controllers;

use App\Orgao;
use Illuminate\Http\Request;

class OrgaoController extends Controller
{
    public function list()
    {
        $orgaos = Orgao::all();

        return response()->json($orgaos);
    }
}

<?php

namespace App\Http\Controllers;

use App\Questao;
use Illuminate\Http\Request;

class QuestaoController extends Controller
{
    public function list()
    {
        $questoes = Questao::all();

        return response()->json($questoes);
    }
}

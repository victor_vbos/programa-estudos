<?php

namespace App\Http\Controllers;

use App\Assunto;
use App\Questao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AssuntoController extends Controller
{
    public function list(Request $request)
    {
        $banca = $request->get('banca');
        $orgao = $request->get('orgao');

        $questoes = Questao::with('assunto')
            ->where('banca_id', '=', $banca)
            ->where('orgao_id', '=', $orgao)
            ->get();
        return response()->json($questoes);
    }
}

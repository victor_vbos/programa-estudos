<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nome
 * @property integer $pai_id
 * @property string $data_exclusao
 * @property string $created_at
 * @property string $updated_at
 * @property Questao[] $questaos
 * @property Assunto[] $filhos
 */
class Assunto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assunto';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['pai'];

    /**
     * @var array
     */
    protected $fillable = ['nome', 'pai_id', 'data_exclusao', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questaos()
    {
        return $this->hasMany('App\Questao');
    }

    public function pai()
    {
        return $this->belongsTo(self::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filhos()
    {
        return $this->hasMany(self::class, 'pai_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nome
 * @property string $data_exclusao
 * @property string $created_at
 * @property string $updated_at
 * @property Questao[] $questaos
 */
class Orgao extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'orgao';

    /**
     * @var array
     */
    protected $fillable = ['nome', 'data_exclusao', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questaos()
    {
        return $this->hasMany('App\Questao');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $assunto_id
 * @property int $orgao_id
 * @property int $banca_id
 * @property string $nome
 * @property string $data_exclusao
 * @property string $created_at
 * @property string $updated_at
 * @property Assunto $assunto
 * @property Banca $banca
 * @property Orgao $orgao
 */
class Questao extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questao';

//    protected $with = ['assunto'];

    /**
     * @var array
     */
    protected $fillable = ['assunto_id', 'orgao_id', 'banca_id', 'nome', 'data_exclusao', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assunto()
    {
        return $this->belongsTo('App\Assunto');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function banca()
    {
        return $this->belongsTo('App\Banca');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function orgao()
    {
        return $this->belongsTo('App\Orgao');
    }
}

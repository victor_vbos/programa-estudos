# Programa de estudos
## Projeto para criação de programas de estudos para concursos.

### Stack

- PHP 7.4.*
- Laravel 7.*
- MySQL 8.0.*
- VueJS 2.*
- Docker

### Inicialização do projeto

1. Clone o repositório:
    `git clone git clone https://victor_vbos@bitbucket.org/victor_vbos/programa-estudos.git && cd programa-estudos`
2. Faça o build das imagens do docker:
    `docker-compose up --build -d`
3. Acesse a aplicação:
    `localhost:82`